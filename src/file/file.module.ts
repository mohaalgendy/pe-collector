import { Module } from '@nestjs/common';
import { FileService } from './file.service';
import { FileController } from './file.controller';
import { RedisService } from '../redis/redis.service';
import { RabbitMQService } from '../rabbitmq/rabbitmq.service';
import { FileQueueService } from './file.queue.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { FileRecord } from '../file-record/entities/file-record.entity';
import { FileRecordService } from '../file-record/file-record.service';
import { S3BucketService } from '../s3-bucket/s3-bucket.service';

@Module({
  imports: [SequelizeModule.forFeature([FileRecord])],
  providers: [
    FileService,
    RedisService,
    RabbitMQService,
    FileQueueService,
    FileRecordService,
    S3BucketService,
  ],
  controllers: [FileController],
})
export class FileModule {}
