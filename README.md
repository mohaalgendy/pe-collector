# Project: Portable Executable File Collector

## Deployed Application on AWS EC2
- **URL**: [http://ec2-16-170-35-254.eu-north-1.compute.amazonaws.com//](http://ec2-16-170-35-254.eu-north-1.compute.amazonaws.com/)

## Overview
This project is a robust file collector system, specifically designed for portable executable files. It leverages Nest.js with Node.js for the backend and utilizes a simple three-page HTML frontend. The core functionality revolves around processing and validating uploaded portable executable files, ensuring optimized handling and storage.

## Key Features
- **File Upload**: Users can upload files through a straightforward interface.
- **Message Queuing**: Uploaded files are queued in a RabbitMQ queue, allowing asynchronous processing and efficient request handling.
- **File Validation**: A dedicated service analyzes each file to determine its uniqueness and validity as a portable executable.
- **Data Storage**: Valid files are recorded in an SQLite database and physically stored in an AWS S3 Bucket.

## Technical Workflow
1. **File Upload**: The user uploads a portable executable file via the frontend.
2. **Queue Management**: The file is sent to a RabbitMQ queue, offloading immediate processing demands from the receiver service.
3. **File Processing**: A separate service listens to the queue, retrieves the file, and performs several checks:
    - Determines if the file is a duplicate by comparing its hash with existing hashes in Redis.
    - Validates the file's content to confirm it is a portable executable, addressing potential mime type spoofing.
4. **Data Recording**: If the file is new and valid, its information is stored in an SQLite database, and the file itself is saved to an AWS S3 Bucket.
5. **User Feedback**: Users can view the hash of the uploaded file in Redis and access file details, including the download path, in the SQLite database.

## Frontend
The frontend comprises three simple HTML pages, offering a user-friendly interface for file upload and viewing file statuses.

## Backend
The backend utilizes Nest.js with Node.js, ensuring a scalable and maintainable codebase. It includes two main services:
- **Receiver Service**: Handles initial file uploads and queues them in RabbitMQ.
- **Processor Service**: Listens to the queue, processes files, and interacts with Redis and SQLite for data management.

## Prerequisites
- Docker and Docker Compose are required to run the application and its dependencies without the need for local installations.

## Getting Started
1. **Clone the repository**:
   ```bash
   git clone <repository-url>

1. **Run the image**:
   ```bash
   docker-compose up --build
   
