import { Injectable } from '@nestjs/common';
import { CreateFileRecordDto } from './dto/create-file-record.dto';
import { FileRecord } from './entities/file-record.entity';
import { InjectModel } from '@nestjs/sequelize';
@Injectable()
export class FileRecordService {
  constructor(
    @InjectModel(FileRecord)
    private fileRecordRepository: typeof FileRecord,
  ) {}

  create(createFileRecordDto: CreateFileRecordDto) {
    return this.fileRecordRepository.create(createFileRecordDto as any);
  }

  findAll() {
    return this.fileRecordRepository.findAll();
  }

  findOne(id: number) {
    return `This action returns a #${id} fileRecord`;
  }

  remove(id: number) {
    return `This action removes a #${id} fileRecord`;
  }
}
