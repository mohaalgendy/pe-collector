import { Controller, Get } from '@nestjs/common';
import { RedisService } from './redis.service';

@Controller('redis')
export class RedisController {
  constructor(private readonly redisService: RedisService) {}

  @Get('keys')
  async listAllKeys(): Promise<string[]> {
    return await this.redisService.listAllKeys();
  }
}
