# Use the official Node.js 20 image as a base
FROM --platform=linux/amd64 node:21

RUN apt-get update && apt-get install --no-install-recommends -y python3 build-essential && apt-get clean

# Set the working directory inside the container
WORKDIR /usr/src/app

# Copy the package.json and package-lock.json (if available)
COPY package*.json ./

# Install the dependencies, including those for production
# Then, explicitly rebuild sqlite3 to ensure compatibility with the container environment
RUN npm install
RUN npm rebuild sqlite3

# Copy the rest of the application code
COPY . .

# Build your NestJS application (adjust this command according to your app's specifics)
RUN npm run build

# Expose the port your app runs on
EXPOSE 3000 5672 15672 6379

CMD ["tail", "-f", "/dev/null"]