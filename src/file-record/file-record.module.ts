import { Module } from '@nestjs/common';
import { FileRecordService } from './file-record.service';
import { FileRecordController } from './file-record.controller';
import { FileRecord } from './entities/file-record.entity';
import { SequelizeModule } from '@nestjs/sequelize';
@Module({
  imports: [SequelizeModule.forFeature([FileRecord])],
  controllers: [FileRecordController],
  providers: [FileRecordService],
})
export class FileRecordModule {}
