import { Injectable, OnModuleInit } from '@nestjs/common';
import { connect, Channel, Connection } from 'amqplib';
import { RABBITMQ_URL, RABBITMQ_QUEUE } from '../config/app-options.constants';
import { FileService } from '../file/file.service';
@Injectable()
export class RabbitMQService implements OnModuleInit {
  private connection: Connection;

  constructor(private readonly fileService: FileService) {}

  async onModuleInit() {
    await this.connectToRabbit();
    await this.consumeMessages();
  }

  private async connectToRabbit() {
    this.connection = await connect(RABBITMQ_URL);
  }

  private async consumeMessages() {
    const channel: Channel = await this.connection.createChannel();
    await channel.assertQueue(RABBITMQ_QUEUE);
    await channel.consume(RABBITMQ_QUEUE, (msg) => {
      if (msg !== null) {
        console.log(`Received message from queue "${RABBITMQ_QUEUE}":`);
        this.fileService.consumeFile(msg.content.toString());
        channel.ack(msg);
      }
    });
  }

  async publishMessage(queue: string, message: string): Promise<void> {
    const channel: Channel = await this.connection.createChannel();
    await channel.assertQueue(queue);
    channel.sendToQueue(queue, Buffer.from(message));
    console.log(`Message sent to queue "${queue}"`);
    await channel.close();
  }
}
