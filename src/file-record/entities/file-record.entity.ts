import { Column, Table, Model } from 'sequelize-typescript';

@Table({
  tableName: 'file-record',
})
export class FileRecord extends Model {
  @Column
  encoding: string;

  @Column
  name: string;

  @Column
  size: string;

  @Column
  mimetype: string;

  @Column
  hash: string;

  @Column
  path: string;
}
