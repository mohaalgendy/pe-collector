export class CreateFileRecordDto {
  encoding: string;
  name: string;
  size: string;
  mimetype: string;
  hash: string;
  path?: string;
}
