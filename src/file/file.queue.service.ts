import { Injectable } from '@nestjs/common';
import { createHash } from 'crypto';
import { RabbitMQService } from '../rabbitmq/rabbitmq.service';
import { FileModel } from './file.interface';
import { RABBITMQ_QUEUE } from '../config/app-options.constants';
type File = Express.Multer.File;
@Injectable()
export class FileQueueService {
  constructor(private readonly rabbitMQService: RabbitMQService) {}
  async queueFile(file: File) {
    if (!file || !file.buffer || file.buffer.length === 0) return;

    const newFile: FileModel = {
      buffer: file.buffer.toString('base64'),
      encoding: file.encoding,
      mimetype: file.mimetype,
      name: file.originalname,
      size: file.size.toString(),
      hash: this.generateFileHash(file.buffer),
    };

    await this.rabbitMQService.publishMessage(
      RABBITMQ_QUEUE,
      JSON.stringify(newFile),
    );
  }
  private generateFileHash(fileData: Buffer): string {
    const hash = createHash('sha256');
    hash.update(fileData);
    return hash.digest('hex');
  }
}
