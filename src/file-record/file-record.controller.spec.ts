import { Test, TestingModule } from '@nestjs/testing';
import { FileRecordController } from './file-record.controller';
import { FileRecordService } from './file-record.service';

describe('FileRecordController', () => {
  let controller: FileRecordController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FileRecordController],
      providers: [FileRecordService],
    }).compile();

    controller = module.get<FileRecordController>(FileRecordController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
