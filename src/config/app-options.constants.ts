import { CacheModuleAsyncOptions } from '@nestjs/cache-manager';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { redisStore } from 'cache-manager-redis-store';
export const RABBITMQ_URL: string = 'amqp://rabbitmq:5672';
export const RABBITMQ_QUEUE: string = 'malwareBytesPECollector';
export const REDIS_URL: string = 'redis://redis:6379';
export const S3_BUCKET_PUBLIC_PATH: string = 'https://anotherbucket6565.s3.eu-north-1.amazonaws.com/';
export const TTL: number = 60 * 60 * 4; // one hour
export const RedisOptions: CacheModuleAsyncOptions = {
  isGlobal: true,
  imports: [ConfigModule],
  useFactory: async () => {
    const store = await redisStore({
      url: REDIS_URL,
    });
    return {
      store: () => store,
    };
  },
  inject: [ConfigService],
};
