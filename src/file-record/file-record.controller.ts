import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FileRecordService } from './file-record.service';
import { CreateFileRecordDto } from './dto/create-file-record.dto';
import { UpdateFileRecordDto } from './dto/update-file-record.dto';

@Controller('file-record')
export class FileRecordController {
  constructor(private readonly fileRecordService: FileRecordService) {}

  @Post()
  create(@Body() createFileRecordDto: CreateFileRecordDto) {
    return this.fileRecordService.create(createFileRecordDto);
  }

  @Get()
  findAll() {
    return this.fileRecordService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fileRecordService.findOne(+id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.fileRecordService.remove(+id);
  }
}
