import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { FileModule } from './file/file.module';
import { FileService } from './file/file.service';
import { RedisService } from './redis/redis.service';
import { RedisModule } from './redis/redis.module';
import { RedisOptions } from './config/app-options.constants';
import { ConfigModule } from '@nestjs/config';
import { CacheModule } from '@nestjs/cache-manager';
import { RabbitMQModule } from './rabbitmq/rabbitmq.module';
import { RabbitMQService } from './rabbitmq/rabbitmq.service';
import { dataBaseConfig } from './database/database.config';
import { SequelizeModule } from '@nestjs/sequelize';
import { FileRecordModule } from './file-record/file-record.module';
import { FileRecordService } from './file-record/file-record.service';
import { FileRecord } from './file-record/entities/file-record.entity';
import { S3BucketModule } from './s3-bucket/s3-bucket.module';
import { S3BucketService } from './s3-bucket/s3-bucket.service';

@Module({
  imports: [
    FileModule,
    ConfigModule.forRoot({ isGlobal: true }),
    CacheModule.registerAsync(RedisOptions),
    SequelizeModule.forRoot(dataBaseConfig),
    SequelizeModule.forFeature([FileRecord]),
    RedisModule,
    RabbitMQModule,
    FileRecordModule,
    S3BucketModule,
  ],
  providers: [
    FileService,
    RabbitMQService,
    AppService,
    RedisService,
    FileRecordService,
    S3BucketService,
  ],
})
export class AppModule {}
