import { Injectable } from '@nestjs/common';
import { RedisService } from '../redis/redis.service';
import { FileModel } from './file.interface';
import { FileRecordService } from '../file-record/file-record.service';
import { S3BucketService } from '../s3-bucket/s3-bucket.service';
import { S3_BUCKET_PUBLIC_PATH } from '../config/app-options.constants';
import { fromBuffer } from 'file-type';

@Injectable()
export class FileService {
  constructor(
    private readonly redisService: RedisService,
    private readonly fileRecordService: FileRecordService,
    private readonly s3BucketService: S3BucketService,
  ) {}

  async consumeFile(stringFile: string) {
    const newFile: FileModel = JSON.parse(stringFile);
    await this.processFile(newFile);
  }

  private async processFile(newFile: FileModel) {
    const fileContent: Buffer = Buffer.from(newFile.buffer, 'base64');
    const hash: string = newFile.hash;
    const realType = await fromBuffer(fileContent);
    newFile.mimetype = `${realType.mime} | *.${realType.ext}`;
    if (await this.redisService.get(hash)) {
      console.log(
        `File named: ${newFile.name} with hash ${hash} already exists`,
      );
      return;
    }
    const isPE = this.isPEFile(fileContent); // Check if file is a PE file

    if (isPE) {
      console.log(`File named: ${newFile.name} with hash ${hash} is a PE file`);
      newFile.path = S3_BUCKET_PUBLIC_PATH + newFile.name;
      await this.fileRecordService.create(newFile); // Save file to database
      await this.s3BucketService.uploadFile(newFile); // Save file to S3
      await this.redisService.set(hash, fileContent.toString('base64'));
    }
  }

  private isPEFile(fileData: Buffer): boolean {
    if (!fileData || fileData.length < 64) {
      return false;
    }

    // Check for "MZ" signature at the beginning
    if (fileData[0] !== 0x4d || fileData[1] !== 0x5a) {
      return false;
    }

    // Retrieve the PE header offset
    const peOffset = fileData.readUInt32LE(60);

    // Check if the offset is within the file bounds
    if (peOffset < 0 || peOffset + 4 >= fileData.length) {
      return false;
    }

    // Check for "PE\0\0" signature at the offset
    return (
      fileData[peOffset] === 0x50 &&
      fileData[peOffset + 1] === 0x45 &&
      fileData[peOffset + 2] === 0x00 &&
      fileData[peOffset + 3] === 0x00
    );
  }
}
