import { Inject, Injectable } from '@nestjs/common';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { TTL } from '../config/app-options.constants';
@Injectable()
export class RedisService {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  async set(key: string, value: string): Promise<void> {
    await this.cacheManager.set(key, value, TTL);
  }

  async get(key: string): Promise<string> {
    return await this.cacheManager.get(key);
  }

  async listAllKeys(): Promise<string[]> {
    return await this.cacheManager.store.keys();
  }

  async del(key: string): Promise<void> {
    await this.cacheManager.del(key);
  }
}
