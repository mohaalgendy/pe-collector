import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { FileModel } from '../file/file.interface';

@Injectable()
export class S3BucketService {
  AWS_S3_BUCKET = 'anotherbucket6565';
  s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  });
  constructor() {}

  async uploadFile(file: FileModel) {
    console.log(`Saving file to S3: ${file.name}`);
    return await this.s3_upload(
      Buffer.from(file.buffer, 'base64'),
      this.AWS_S3_BUCKET,
      file.name,
      file.mimetype,
    );
  }

  async s3_upload(file: any, bucket: string, name: any, mimetype: any) {
    const params = {
      Bucket: bucket,
      Key: String(name),
      Body: file,
      ACL: 'public-read',
      ContentType: mimetype,
      ContentDisposition: 'inline',
      CreateBucketConfiguration: {
        LocationConstraint: 'eu-north-1',
      },
    };

    try {
      return await this.s3.upload(params).promise();
    } catch (e) {
      console.log(e);
    }
  }
}
