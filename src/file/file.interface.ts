export interface FileModel {
  buffer: string;
  encoding: string;
  mimetype: string;
  name: string;
  size: string;
  hash: string;
  path?: string;
}
