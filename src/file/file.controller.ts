import {
  Controller,
  Post,
  Res,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';
import { FileQueueService } from './file.queue.service';
import { FileRecordService } from '../file-record/file-record.service';
type File = Express.Multer.File;

@Controller('api')
export class FileController {
  constructor(
    private readonly fileQueueService: FileQueueService,
    private readonly fileRecordService: FileRecordService,
  ) {}

  @Post('fileUpload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file: File, @Res() res: Response) {
    await this.fileQueueService.queueFile(file);
    return res.status(200).send(true);
  }
}
