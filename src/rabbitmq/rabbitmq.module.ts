import { Module } from '@nestjs/common';
import { RabbitMQService } from './rabbitmq.service';
import { FileService } from '../file/file.service';
import { RedisService } from '../redis/redis.service';
import { FileRecordService } from '../file-record/file-record.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { FileRecord } from '../file-record/entities/file-record.entity';
import { S3BucketService } from '../s3-bucket/s3-bucket.service';

@Module({
  imports: [SequelizeModule.forFeature([FileRecord])],
  providers: [
    RabbitMQService,
    FileService,
    RedisService,
    FileRecordService,
    S3BucketService,
  ],
  exports: [RabbitMQService],
})
export class RabbitMQModule {}
